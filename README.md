# helloRaspberry

An automated installer for configuring helloSystem's helloDesktop on Raspberry Pi's running FreeBSD 13. It simplifies the install process for those with this magical little device.

# Usability statement

This is pre-production software. It comes with no warranty. It is intended for use by experienced users who are willing to deal with data loss and other issues. Expect bugs. If something goes wrong in the install process, it is a "helloRaspberry" issue. If you successfully install your systembut experience bugs, it is a helloSystem issue. Please report bugs related to this software [here](https://gitlab.com/trivoxel-utils/raspberry/issues), and helloSystem bugs [here](https://github.com/hellosystem/hello/issues).

Please note, this is UNTESTED SOFTWARE. HelloSystem is developed for Intel/AMD processors. This is a simple port to ARM64 so expect issues. It is unknown how stable the experience will be.

## Getting started

Please refer to the official docs @ https://helloSystem.github.io/docs/rpi.html for more information about installing your system.

## Using this script

After installing [FreeBSD 13](https://freebsd.org/where) to your Raspberry Pi's SD card or SATA SSD (optional), please do the following steps to use this program:

1. Ensure your device has a stable internet connection. It is recommended to use Ethernet when running FreeBSD on a Raspberry Pi
2. Make sure you are in a userland configuration and are not running under Root. A password prompt will show up when Root access is needed.
3. You have sudo configured.
4. Run this script by typing `helloraspberry.sh --install` into your command line.

> **Note:** These steps are subject to change. Automatic sudo configuration is a planned feature.
