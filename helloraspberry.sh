#!/bin/env tcsh

## Simple installer utility for helloSystem desktop
## Created by Caden Mitchell | (C) 2022 Caden Mitchell/TriVoxel
## See BSD 2 license for more details
## TODO legend: P=Priority from high to low

_version="0.0" # Yeah, this thing has  only just begun
_author="TriVoxel"

_configureSudo () { # TODO: @TriVoxel P=low
    su root
    echo "Updating system in3 seconds... (Ctrl+C to cancel)"
    freebsd-update fetch install
    pkg update && pkg upgrade
    pkg install -y sudo
    # Configure "wheel" group stuff <...>
}

_installDeps () {
    sudo pkg install -y "\
git \
cmake \
pkgconf \
qt5-qmake \
qt5-buildtools \
kf5-kdbusaddons \
kf5-kwindowsystem \
libdbusmenu-qt5 \
qt5-concurrent \
qt5-quickcontrols2 \
libfm \
libqtxdg \
wget"

    depsUrl="https://raw.githubusercontent.com/helloSystem/ISO/experimental/settings/packages.hello"
    curl -fsS $depsUrl | egrep -v '^#' | while read line ; do
        pkg install -y -U $line || echo "[error] $line cannot be installed"
    done
}

echo "Sorry. This software is still under development. Check back later."
# This will need a lot of testing before it is fully ready. If you are testing this before it has reached "beta" stage or later, it is very likely to completely break in unforseeable ways. That bleeding edge, baby!
